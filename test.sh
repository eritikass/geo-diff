#!/usr/bin/env bash

# register some help calls
curl "http://localhost:3000/help/57.4385391/24.763323699999997?message=need+some+help1"
curl "http://localhost:3000/help/59.4385391/24.763323699999997?message=need+some+help2" # closest
curl "http://localhost:3000/help/62.4385391/24.763323699999997?message=need+some+help3"

echo "\n"

# find closest need of help
curl "http://localhost:3000/find/59.4385191/24.76332360"

echo "\n"
curl "http://localhost:3000/status/1"

echo "\n"

# offer help
curl "http://localhost:3000/offer/1/siim"

# check status
echo "\n"
curl "http://localhost:3000/status/1"
