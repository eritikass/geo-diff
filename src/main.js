const express = require('express');
const geolib = require('geolib');

const sum = require('./sum');

const app = express();

const port = process.env.PORT || 3000;

const cords = [];

const getGeolocation = req => {
  const { latitude, longitude } = req.params;
  return {
    latitude: parseFloat(latitude),
    longitude: parseFloat(longitude),
  };
};

app.get('/help/:latitude/:longitude', (req, res) => {
  const geolocation = getGeolocation(req);
  const { message = '' } = req.query;
  const index =
    cords.push({
      message,
      geolocation,
      time: +new Date(),
    }) - 1;
  res.json({
    index,
    ok: true,
  });
});

app.get('/find/:latitude/:longitude', (req, res) => {
  const geolocation = getGeolocation(req);
  let closest = null;
  cords.forEach((message, index) => {
    const distance = geolib.getDistance(geolocation, message.geolocation);
    if (closest === null || closest.distance > distance) {
      closest = {
        index,
        message,
        distance,
      };
    }
  });
  res.json({
    closest,
  });
});

app.get('/offer/:id/:name', (res, req) => {
  const { id, name } = req;
  const id2 = parseInt(id, 10);
  if (cords[id2] && cords[id2].offer) {
    cords[id2].offer = {
      name,
    };
  }
  res.json({
    ok: true,
  });
});

app.get('/status/:id', (res, req) => {
  const { id } = req;
  res.json(cords[parseInt(id, 10)]);
});

app.get('/tester', (req, res) => {
  res.json(cords);
});

app.listen(port, () => {
  console.log(`Server is now running on http://localhost:${port}/`);
});
